import React, {useState,useEffect,useContext} from 'react';

import {
	Container,
	Form,
	Button

} from 'react-bootstrap';

//state
import UserContext from './../UserContext';
// import {useHistory} from 'react-router-dom';
import {useHistory,Redirect} from 'react-router-dom';

import Swal from 'sweetalert2';


export default function Register() {
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [verifyPassword, setVerifyPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

	const {user} = useContext(UserContext);
	let history = useHistory();
	useEffect(()=>{
		if (email !== '' && password !== '' && verifyPassword !== '' && password === verifyPassword) {
			setIsDisabled(false)


		} else {
			setIsDisabled(true)


		}

	}, [email, password, verifyPassword]);

	function register(e) {
		e.preventDefault();

		fetch("https://fast-bastion-88049.herokuapp.com/api/users/checkEmail", 
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					email: email
				})

			}
		)
		.then(result => result.json())
		.then(result => {
			// console.log(result)
			if (result === true) {
				Swal.fire({
					title: 'Duplicate email found',
					icon: 'error',
					text: 'Please choose another email'
				})

				// history.push('/register');
			} else {
				fetch("https://fast-bastion-88049.herokuapp.com/api/users/register",
					{
						method: "POST",
						headers: {
							"Content-Type": "application/json"
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							mobileNo: mobileNo,
							email: email,
							password: password
						})
					}
				)
				.then( result => result.json())
				.then( result => {
					console.log(result)
					if(result === true){
						Swal.fire({
							title: 'Registered Successfully',
							icon: 'success',
							text: 'Welcome to PickMe Shop'
						})

						history.push('/login');
					} else {
						
						Swal.fire({
							title: 'Something went wrong',
							icon: 'error',
							text: 'Please try again'
						})
					}
				})
			}
		})




		// alert('Registered Succesfully!')
		//update
		// setUser({email: email});
		// localStorage.setItem('email',email);
		// setFirstName('');
		// setLastName('');
		// setMobileNo('');
		// setEmail('');
		// setPassword('');
		// setVerifyPassword('');
		// history.push('/login');


	}
	// if (user.email !== null) {
	// 	unsetUser();
	// 	return <Redirect to="/login"/>
	// }

	return (
		(user.id !== null)
		? <Redirect to="/"/>
		: <Container className="my-5">
			<h1 className="text-center">Register</h1>
			<Form onSubmit={(e) => register(e)}>

			  <Form.Group className="mb-3" controlId="formBasicFirstName">
			    <Form.Label>First Name</Form.Label>
			    <Form.Control type="text" placeholder="Enter First Name" value = {firstName}
			    onChange = {(e) => setFirstName(e.target.value)}/>
			  </Form.Group> 

			  <Form.Group className="mb-3" controlId="formBasicLastName">
			    <Form.Label>Last Name</Form.Label>
			    <Form.Control type="text" placeholder="Enter Last Name" value = {lastName}
			    onChange = {(e) => setLastName(e.target.value)}/>
			  </Form.Group> 

			  <Form.Group className="mb-3" controlId="formBasicMobileNo">
			    <Form.Label>Mobile Number</Form.Label>
			    <Form.Control type="number" placeholder="Enter Mobile Number" value = {mobileNo}
			    onChange = {(e) => setMobileNo(e.target.value)}/>
			  </Form.Group> 

			  <Form.Group className="mb-3" controlId="formBasicEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control type="email" placeholder="Enter email" value = {email}
			    onChange = {(e) => setEmail(e.target.value)}/>
			  </Form.Group> 

			  <Form.Group className="mb-3" controlId="formBasicPassword">
			    <Form.Label>Password</Form.Label>
			    <Form.Control type="password" placeholder="Password" value = {password} onChange = {(e) => setPassword(e.target.value)}/>
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="formVerifyPassword">
			    <Form.Label>Verify Password</Form.Label>
			    <Form.Control type="password" placeholder="Verify Password" value = {verifyPassword} onChange = {(e) => setVerifyPassword(e.target.value)}/>
			  </Form.Group>

			  <Button variant="primary" type="submit" disabled={isDisabled}>
			    Submit
			  </Button>
			</Form>
		</Container>
		)



}