import React, {useState, useEffect,useContext} from 'react';
/*react-bootstrap component*/
import {Container} from 'react-bootstrap'

/*components*/
// import Course from './../components/Course';
import AdminView from './../components/AdminView';
import UserView from './../components/UserView';
import UserContext from './../UserContext';
/*mock data*/
// import courses from './../mock-data/courses';


export default function Courses(){

	const [courses, setCourses] = useState([]);
	const {user} = useContext(UserContext);
	const fetchData = () => {
		let token = localStorage.getItem('token')
		console.log(token);
		fetch('https://fast-bastion-88049.herokuapp.com/api/products/all',{ //pang user lang to
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)
			setCourses(result)
		})
	}

	useEffect( () => {
		fetchData()
	}, [])

	// let CourseCards = courses.map( (course) => {
	// 	return <Course key={course.id} course={course}/>
	// })
 
	return(
		<Container className="p-4">
			{/*{CourseCards}*/}
			{ (user.isAdmin===true) 
				? <AdminView courseData = {courses} fetchData={fetchData}/>
				: <UserView courseData = {courses}/>
			}
		</Container>
	)
}
