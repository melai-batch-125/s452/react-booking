import React, {useState,useEffect,useContext} from 'react';

import {
	Container,
	Form,
	Button

} from 'react-bootstrap';

//state

//context
import UserContext from './../UserContext';

import {Redirect} from 'react-router-dom';


export default function Login() {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

	//destructure context object
	const {user,setUser} = useContext(UserContext);

	useEffect(()=>{
		if (email !== '' && password !== '') {
			setIsDisabled(false)


		} else {
			setIsDisabled(true)


		}

	}, [email, password]);

	function login(e) {
		e.preventDefault();

		fetch('https://fast-bastion-88049.herokuapp.com/api/users/login',
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					email: email,
					password: password
				})

			}
		)
		.then(result => result.json())
		.then(result => {
			console.log(result); //access:token

			if(typeof result.access !== "undefined") 
			{
				localStorage.setItem("token", result.access);
				userDetails(result.access);
			}


		})

		

		const userDetails = (token) => {
			fetch('https://fast-bastion-88049.herokuapp.com/api/users/details', 
				{
					method: "GET",
					headers: {
						"Authorization": `Bearer ${token}`
					}
				}
			)
			.then(result => result.json())
			.then(result => {

				console.log(result)
				setUser({
					id: result._id,
					isAdmin: result.isAdmin
				})

				// localStorage.setItem("id", result._id);
				// localStorage.setItem("isAdmin", result.isAdmin);

				// window.location.replace('./courses.html')


			})
		}


		// alert('Successfully logged in!')

		//update
		setUser({email: email});
		localStorage.setItem('email',email);

		setEmail('');
		setPassword('');

	}

	// if (user.email !== null) {
	// 	return <Redirect to="/"/>
	// }
	return (
		(user.id !== null)
		? <Redirect to="/"/>

		: <Container className="my-5">
			<h1 className="text-center">Login</h1>
			<Form  onSubmit={(e) => login(e)}>
			  <Form.Group className="mb-3" controlId="formBasicEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control type="email" placeholder="Enter email" value = {email}
			    onChange = {(e) => setEmail(e.target.value)}/>
			  </Form.Group> 

			  <Form.Group className="mb-3" controlId="formBasicPassword">
			    <Form.Label>Password</Form.Label>
			    <Form.Control type="password" placeholder="Password" value = {password} onChange = {(e) => setPassword(e.target.value)}/>
			  </Form.Group>

			  <Button variant="primary" type="submit" disabled={isDisabled}>
			    Login
			  </Button>
			</Form>
		</Container>



		)

/*
	return (
		<Container className="my-5">
			<h1 className="text-center">Login</h1>
			<Form onSubmit={login}>
			  <Form.Group className="mb-3" controlId="formBasicEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control type="email" placeholder="Enter email" value = {email}
			    onChange = {(e) => setEmail(e.target.value)}/>
			  </Form.Group> 

			  <Form.Group className="mb-3" controlId="formBasicPassword">
			    <Form.Label>Password</Form.Label>
			    <Form.Control type="password" placeholder="Password" value = {password} onChange = {(e) => setPassword(e.target.value)}/>
			  </Form.Group>

			  <Button variant="primary" type="submit" disabled={isDisabled}>
			    Login
			  </Button>
			</Form>
		</Container>



		)*/
}