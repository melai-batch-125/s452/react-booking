import React from 'react';

import {
	Row,
	Col,
	Button,
	Card

} from 'react-bootstrap';



export default function CourseCard() {
	return (
		<Row className = "px-3 mt-3">
			<Col>
				  	<Card>
				  	  <Card.Body>
					  	    <Card.Title>Sample Course</Card.Title>
						  	    <Card.Text>
						  	      Description:
						  	      <br/>
						  	    {/*</Card.Text>*/}
						  	    {/*<Card.Text>*/}
						  	      This is sample course offering.
						  	    </Card.Text>
						  	    <Card.Text>
						  	      Price:
						  	      <br/>
						  	    {/*</Card.Text>*/}
						  	    {/*<Card.Text>*/}
						  	      PhP 40,000
						  	    </Card.Text>
					  	    <Button variant="primary">Enroll</Button>
				  	  </Card.Body>
				  	</Card>
			</Col>
		</Row>



		)
}