import React from 'react';

import {
	Row,
	Col,
	Card

} from 'react-bootstrap';



export default function Highlights() {
  return (
  	/*
  	<Card>
  	  <Card.Body>
	  	    <Card.Title>Card Title</Card.Title>
		  	    <Card.Text>
		  	      Some quick example text to build on the card title and make up the bulk of the card's content.
		  	    </Card.Text>
	  	    <Button variant="primary">Go somewhere</Button>
  	  </Card.Body>
  	</Card>
*/
	<Row className = "px-3">
		<Col xs = {12} md = {4}>
			  	<Card>
			  	  <Card.Body>
				  	    <Card.Title>Learn from Home</Card.Title>
					  	    <Card.Text>
					  	       This HTML file is a template.
							      If you open it directly in the browser, you will see an empty page.

							      You can add webfonts, meta tags, or analytics to this file.
							      The build step will place the bundled scripts into the tag.

							      To begin the development, run `npm start` or `yarn start`.
							      To create a production bundle, use `npm run build` or `yarn build`.
					  	    </Card.Text>
				  	    {/*<Button variant="primary">Go somewhere</Button>*/}
			  	  </Card.Body>
			  	</Card>
		</Col>
		<Col xs = {12} md = {4}>
			  	<Card>
			  	  <Card.Body>
				  	    <Card.Title>Study Now, Pay Later</Card.Title>
					  	    <Card.Text>
					  	       This HTML file is a template.
							      If you open it directly in the browser, you will see an empty page.

							      You can add webfonts, meta tags, or analytics to this file.
							      The build step will place the bundled scripts into the tag.

							      To begin the development, run `npm start` or `yarn start`.
							      To create a production bundle, use `npm run build` or `yarn build`.
					  	    </Card.Text>
				  	    {/*<Button variant="primary">Go somewhere</Button>*/}
			  	  </Card.Body>
			  	</Card>
		</Col>
		<Col xs = {12} md = {4}>
			  	<Card>
			  	  <Card.Body>
				  	    <Card.Title>Be Part of the Community</Card.Title>
					  	    <Card.Text>
					  	       This HTML file is a template.
							      If you open it directly in the browser, you will see an empty page.

							      You can add webfonts, meta tags, or analytics to this file.
							      The build step will place the bundled scripts into the tag.

							      To begin the development, run `npm start` or `yarn start`.
							      To create a production bundle, use `npm run build` or `yarn build`.
					  	    </Card.Text>
				  	    {/*<Button variant="primary">Go somewhere</Button>*/}
			  	  </Card.Body>
			  	</Card>
		</Col>



	</Row>


  )
}