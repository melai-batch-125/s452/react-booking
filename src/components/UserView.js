import React, {useState,useEffect} from 'react';

import {Container,Col} from 'react-bootstrap';

import Course from './Course';

export default function UserView({courseData}) {
	console.log(courseData)
	const [courses, setCourses] = useState([]);

	useEffect(()=>{
		const courseArr = courseData.map((course)=>{
			if (course.isActive === true ) {
				return <Course key={course._id} courseProp={course}/>
			} else {
				return null
			}
		})

		setCourses(courseArr);


	},[courseData])


	return (
		<Col>
			<Container>
				{courses}
			</Container>
		</Col>
		)
}