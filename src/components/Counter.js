import React, {useState} from 'react';
import {Container} from 'react-bootstrap'
export default function Counter(){

	const [count,setCount] = useState(0);
	return (
		<Container>
			<h1>You clicked {count} times</h1>
			<button className="btn btn-primary" onClick={
				()=>{
					setCount(count+1)
				}



			}>Click Me</button>
		</Container>

		)
}