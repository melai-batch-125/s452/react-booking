import React, {useState,useEffect} from 'react';
 import {
 	Card,
 	Button,
 	Col
 } from 'react-bootstrap';

 import PropTypes from 'prop-types';

export default function Course({courseProp}) {
	// console.log(props.key)
	// let course = props.courseProp
	console.log(courseProp)
	const {name,description,price} = courseProp //why destructing

	//state
	const [count, setCount] = useState(0);
	const [countSlot, setCountSlot] = useState(10);
	const [isDisabled, setIsDisabled] = useState(false);

	function enroll() {
		if(countSlot > 0) 
    	{
    		setCount(count + 1)
    		setCountSlot(countSlot - 1)

    	}
    	
	}

	useEffect(()=>{
		if (countSlot===0) 
		{
			setIsDisabled(true)
		}
	},[countSlot])



	return (
		<Col xs = {12} md = {4}>
		  	<Card>
		  	  <Card.Body>
			  	    <Card.Title>{name}</Card.Title>
				  	    <h5>Description:</h5>
				  	    <p>{description}</p>
				  	    <h5>Price:</h5>
				  	    <p>{price}</p>
				  	    <h5>Enrollees: {count}</h5>
				  	    <h5>Slot: {countSlot}</h5>
			  	    <Button variant="primary" onClick={ enroll
			  	    	/*() => {
			  	    	count < 30 
			  	    	? setCount(count + 1) //if true yung condition
			  	    	: alert('alert') // else to
			  	   			 }*/


			  	} disabled={isDisabled}>Enroll</Button>
		  	  </Card.Body>
		  	</Card>
		 </Col>
		)
}

Course.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}