import React, {Fragment,useContext} from 'react';


import UserContext from './../UserContext';


import {Nav, Navbar} from 'react-bootstrap';
import {Link, NavLink,useHistory} from 'react-router-dom';

/*app navbar*/

export default function AppNavbar(){

  // console.log(props);

  const {user,unsetUser} = useContext(UserContext);

  let history = useHistory();

  const logout = () => {
    unsetUser();
    history.push('/login');
  }

  let leftNav = (user.id !== null) ? 
          (user.isAdmin === true) ?
            <Fragment>
              <Nav.Link as={NavLink} to="/addCourse">Add Course</Nav.Link>
              <Nav.Link onClick={logout}>Logout</Nav.Link>
            </Fragment>
          :
            <Fragment>
              <Nav.Link onClick={logout}>Logout</Nav.Link>
            </Fragment>
      :
        (
          <Fragment>
              <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
              <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
            </Fragment>

        )



  return (
    <Navbar variant="dark" bg="dark" expand="lg">
      <Navbar.Brand as={Link} to="/">Course Booking</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link as={NavLink} to="/">Home</Nav.Link>
          <Nav.Link as={NavLink} to = "/courses">Courses</Nav.Link>
          {leftNav}
        </Nav>
      </Navbar.Collapse>
    </Navbar> 
    )
}