import React from 'react';
import {Container,Row,Col,Jumbotron} from 'react-bootstrap'
export default function ErrorPage(){

	return (
		<Container fluid>
			<Row>
				<Col className="px-0">
					<Jumbotron fluid className="px-3">
						<h1 className="ml-1">404 - Not found</h1>
						<p>The page you are looking for cannot be found</p>
						<a href="/">Back home</a>	
					</Jumbotron>
				</Col>
			</Row>		
		</Container>

		)
}