import React, {useState,useEffect} from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
// import Counter from './components/Counter';
import ErrorPage from './components/ErrorPage';
// import CourseCard from './components/CourseCard';

import Courses from './pages/Courses'
import Register from './pages/Register'
import Login from './pages/Login'
import UserContext from './UserContext';

import SpecificCourse from './pages/SpecificCourse';
import AddCourse from './pages/AddCourse';


import {
  BrowserRouter,
  Switch,
  Route
} from "react-router-dom";

export default function App() {

	const [user,setUser] = useState({
		id: null,
		isAdmin: null

	});

	const unsetUser = () => {
		localStorage.clear();
		setUser({id: null,isAdmin: null});

	}

	useEffect(()=>{
		let token = localStorage.getItem('token')
		fetch('https://fast-bastion-88049.herokuapp.com/api/users/details',
			{
				method: "GET",
				headers: {
					"Authorization": `Bearer ${token}`
				}
			}
		)
		.then(result => result.json())
		.then( result => {
			console.log(result)
			// console
			if (typeof result._id !== "undefined") {
				setUser({
					id: result._id,
					isAdmin: result.isAdmin
				})
			} else {
				setUser({
					id: null,
					isAdmin: null
				})
			}
		})


	},[])


	return(
		/*<Fragment>
		  <AppNavbar/>
		  <Home/>
		  <CourseCard/>
		  <Welcome name="John"/>
		  <Welcome name="Lawrence"/>
		  <Courses/>
		  <Counter/>
		  <Register/>
		  <Login/>
		</Fragment> */
		<UserContext.Provider value = {{user, setUser, unsetUser}}>
			<BrowserRouter>
				<AppNavbar/>
					<Switch>
						<Route exact path = "/" component = {Home}/>
						<Route exact path = "/courses" component = {Courses}/>
						<Route exact path = "/register" component = {Register}/>
						<Route exact path = "/login" component = {Login}/>
						<Route exact path="/courses/:courseId" component={SpecificCourse} />
						<Route exact path= "/addCourse" component={AddCourse} />
						<Route component = {ErrorPage}/>
					</Switch>
			</BrowserRouter>
		</UserContext.Provider>


		)



}